from django.db import models
class User(models.Model):
    name = models.CharField('Имя пользователя', max_length=30)
    lastname = models.CharField('Фамилия пользователя', max_length=30)
    email = models.CharField('Email пользователя', max_length=50)
    birth_date = models.DateField('Дата рождения')
    dept = models.CharField('Отдел', max_length=30)
    position = models.CharField('Должность', max_length=30)

    @property
    def countUncompleted(self):
        uncmltd_tasks = Task.objects.filter(user = self, status=0)
        return(len(uncmltd_tasks))

    def __str__(self):
        return self.name + ' ' + self.lastname

class Task(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField('Название задачи', max_length=30)
    desc = models.TextField('Описание задачи', max_length=200)
    status = models.BooleanField('Готовность задачи')
    deadline = models.DateField('Дедлайн')
    completed = models.DateField('Дата выполнения', blank=True, null=True)

    @property
    def comments(self):
        all_comments = Comment.objects.filter(task = self)
        return self.comment_set.all()

    @property
    def overdue(self):
        return self.completed > self.deadline 

    def __str__(self):
        users = User.objects.filter(id = self.user_id)
        users_str = [str(user) for user in users]
        users_str = ', '.join(users_str)
        return self.name + ' for ' + users_str

class Comment(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    author = models.CharField('Author', max_length=30)
    message = models.TextField('Comment message', max_length = 300)

    def __str__(self):
        return self.author + " comment about " + str(self.task)  
    