# Generated by Django 3.1.4 on 2020-12-14 18:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tlist', '0002_auto_20201213_1014'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('author', models.CharField(max_length=30, verbose_name='Автор')),
                ('message', models.TextField(max_length=300, verbose_name='Текст комментария')),
                ('task', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tlist.task')),
            ],
        ),
    ]
