from django.urls import path
from . import views
app_name = 'tlist'
urlpatterns = [
    path('', views.index, name = 'index'),
    path('<int:user_id>/', views.details, name = 'details'),
    path('comment/<int:task_id>/', views.comment, name = 'comment')
]