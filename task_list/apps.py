from django.apps import AppConfig


class TlistConfig(AppConfig):
    name = 'tlist'
