from django.shortcuts import render, redirect
from .models import Task, User, Comment
from .forms import CommentForm

def index(request):
    users = User.objects.all()
    tasks = User.objects.all()
    return render(request, 'tlist.html', {'users' : users, 'tasks' :tasks })

def details(request, user_id):
    user = User.objects.get(id = user_id)
    completed_tasks = Task.objects.filter(user = user_id, status=1).order_by('-completed')
    uncompleted_tasks = Task.objects.filter(user = user_id, status=0).order_by('deadline')
    context  = { 'user' : user, 
                 'completed_tasks' : completed_tasks,
                 'uncompleted_tasks' : uncompleted_tasks }
    return render(request, 'details.html', context)

def comment(request, task_id):
    errors = False
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid:
            form.save()
        else:
            errors = True    
    form = CommentForm(initial={'task':task_id})
    task = Task.objects.get(id = task_id)
    comments = task.comments
    return render(request, 'comment.html', 
                {'form' : form, 'task_id': task_id, 
                'comments': comments})